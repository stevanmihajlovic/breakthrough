(defun konstruktor (m n pom)
  (cond ((or (< m 4) (< n 2)) (format t "Nekorektne dimenzije table"))
        ((equal pom 0) '())
        ((> pom (- m '2)) (cons (popunivrstu n 'x) (konstruktor m n (1- pom))))
        ((or (equal pom '1) (equal pom '2)) (cons (popunivrstu n 'o) (konstruktor m n (1- pom))))
        (t (cons (popunivrstu n '-) (konstruktor m n (1- pom))))))

(defun popunivrstu (n znak)
  (cond ((equal n 0) '())
        (t (cons znak (popunivrstu (1- n) znak)))))



(defun ciljnaprovera (matrica)
  (cond ((null matrica) '())
        ((or (member 'o (car matrica)) (not (nemafigura 'x matrica))) t)
        ((or (member 'x (car (last matrica))) (not (nemafigura 'o matrica))) t)
        (t '())))


(defun nemafigura (znak matrica)
  (cond ((null matrica) '())
        (t (or (member znak (car matrica)) (nemafigura znak (cdr matrica))))))



(defun stampaj (matrica len)
  (cond ((null (cdr matrica)) (format t "~%~a ~a~%  ~a" (- len (length matrica)) (car matrica) (reverse (generisislova(1- (length (car matrica)))))))
        (t (format t "~%~a ~a" (- len (length matrica)) (car matrica)) (stampaj (cdr matrica) len))))

(defun generisislova (ukupanbroj)
  (cond ((equal ukupanbroj -1) '())
        (t (cons (brojukarakter ukupanbroj) (generisislova(1- ukupanbroj))))))



(defun brojukarakter(broj)
  (cond ((equal broj 0) 'a)
        ((equal broj 1) 'b)
        ((equal broj 2) 'c)
        ((equal broj 3) 'd)
        ((equal broj 4) 'e)
        ((equal broj 5) 'f)
        ((equal broj 6) 'g)
        ((equal broj 7) 'h)
        ((equal broj 8) 'i)
        ((equal broj 9) 'j)
        ((equal broj 10) 'k)
        ((equal broj 11) 'l)
        ((equal broj 12) 'm)
        ((equal broj 13) 'n)
        ((equal broj 14) 'o)
        ((equal broj 15) 'p)
        ((equal broj 16) 'q)
        ((equal broj 17) 'r)
        ((equal broj 18) 's)
        ((equal broj 19) 't)
        ((equal broj 20) 'u)
        ((equal broj 21) 'v)
        ((equal broj 22) 'w)
        ((equal broj 23) 'x)
        ((equal broj 24) 'y)
        ((equal broj 25) 'z)
        (t 'bad)))
(defun karakterubroj(karakter)
  (cond ((equal karakter 'a) '0)
        ((equal karakter 'b) '1)
        ((equal karakter 'c) '2)
        ((equal karakter 'd) '3)
        ((equal karakter 'e) '4)
        ((equal karakter 'f) '5)
        ((equal karakter 'g) '6)
        ((equal karakter 'h) '7)
        ((equal karakter 'i) '8)
        ((equal karakter 'j) '9)
        ((equal karakter 'k) '10)
        ((equal karakter 'l) '11)
        ((equal karakter 'm) '12)
        ((equal karakter 'n) '13)
        ((equal karakter 'o) '14)
        ((equal karakter 'p) '15)
        ((equal karakter 'q) '16)
        ((equal karakter 'r) '17)
        ((equal karakter 's) '18)
        ((equal karakter 't) '19)
        ((equal karakter 'u) '20)
        ((equal karakter 'v) '21)
        ((equal karakter 'w) '22)
        ((equal karakter 'x) '23)
        ((equal karakter 'y) '24)
        ((equalp karakter 'z) '25)
        (t '-1)))
        

(defun promenastanja (staro novo matrica Trenutnoigra)
  (let* ((y1 (cadr staro))
         (y2 (cadr novo))
         (x1 (karakterubroj (car staro)))
         (x2 (karakterubroj (car novo))))
  (cond ((or (not (equal (length staro) 2)) (not (equal (length novo) 2))) '())
        ((or (> y1 (1- m)) (< y1 '0)) '())
        ((or (> y2 (1- m)) (< y2 '0)) '())
        ((or (> x1 (1- n)) (< x1 '0)) '())
        ((or (> x2 (1- n)) (< x2 '0)) '())
        ((not (equal (nth x1 (nth y1 matrica)) Trenutnoigra)) '())
        (t (if (equal 'x Trenutnoigra) 
               (cond ((and (equal (1+ y1) y2) (equal x1 x2) (equal (nth x2 (nth y2 matrica)) '-)) t)
                     ((and (equal (1+ y1) y2) (or (equal x2 (1- x1)) (equal x2 (1+ x1))) 
                       (not (equal Trenutnoigra (nth x2 (nth y2 matrica))))) t)
            (t '()))
             (cond ((and (equal (1- y1) y2) (equal x1 x2) (equal (nth x2 (nth y2 matrica)) '-)) t)
                   ((and (equal (1- y1) y2) (or (equal x2 (1- x1)) (equal x2 (1+ x1)))
                       (not (equal Trenutnoigra (nth x2 (nth y2 matrica))))) t)
                   (t '())))))))


(defun promenitablu (x1 y1 x2 y2 tabla Trenutnoigra)
  (cond ((null tabla) '())
        ((zerop x1) (cons (promeni '- y1 (car tabla)) (promenitablu (1- x1) y1 (1- x2) y2 (cdr tabla) Trenutnoigra)))
        ((zerop x2) (cons (promeni Trenutnoigra y2 (car tabla)) (promenitablu (1- x1) y1 (1- x2) y2 (cdr tabla) Trenutnoigra)))
        (t (cons (car tabla) (promenitablu (1- x1) y1 (1- x2) y2 (cdr tabla) Trenutnoigra)))))

(defun promeni (vred j lista)
               (cond ((null lista) '())
                     ((zerop j) (cons vred (cdr lista)))
                     (t (cons (car lista) (promeni vred (1- j) (cdr lista))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;                                                          ;;;;;;;;;;;
;;;;;;;;;;;                     MAIN FUNCTION                        ;;;;;;;;;;;
;;;;;;;;;;;                                                          ;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun main ()
  (format t "Unesite broj vrsta: ")
  (setq m (read))
  (format t "~%Unesite broj kolona: ")
  (setq n (read))
  ;;ubaci kad igra komp/igrac
  (format t "~%Unesite znak kompa: ")
  (setq komp (read))
  (if (equal komp 'o) (setq dubinaalfabeta '3) (setq dubinaalfabeta '2))
  (format t "~%Tabla izgleda:")
  (stampaj (setq matrica (konstruktor m n m)) (length matrica)) 
  (whilepetlja matrica 'x))


;;(defun whilepetlja (tabla napotezu)
;;  (if (ciljnaprovera tabla) (progn (format t "~%Kraj igre! Pobedio je ~a!" (protivnik napotezu)))
;;    (if (equal napotezu komp) (progn (setq trenutnostanje tabla)
;;                              (let ((novatablakomp (car (minimax-alfabeta tabla '5 -1000 1000 napotezu)))) (progn (stampaj novatablakomp m) (whilepetlja novatablakomp (protivnik napotezu))))) ;;izmedju stampaj i while petlje umetni setq trenutno stanje table 
;;    (progn (format t "~%Na potezu je ~a, unesite potez: ~%" napotezu )
;;     (let*((staro (read))
;;           (novo (read))
;;           (novatabla (promenitablu (cadr staro) (karakterubroj (car staro)) (cadr novo) (karakterubroj (car novo)) tabla napotezu)))
;;       (if (promenastanja staro novo tabla napotezu) (progn (format t "~%Tabla nakon poteza izgleda: ") (stampaj novatabla m) (whilepetlja novatabla (protivnik napotezu))) 
;;         (progn (format t "~%Nemoguc potez!~%") (whilepetlja tabla napotezu))))))))



(defun whilepetlja (tabla napotezu)
  (if (ciljnaprovera tabla) (progn (format t "~%Kraj igre! Pobedio je ~a!" (protivnik napotezu)))
    (if (equal napotezu komp) (progn (setq trenutnostanje tabla)
                              (let* ((stanjeheur (minimax-alfabeta tabla dubinaalfabeta -1000 1000 napotezu)) (novatablakomp (car stanjeheur)) (heur (cadr stanjeheur))) (progn (format t "~%~a~%" heur) (stampaj novatablakomp m) (whilepetlja novatablakomp (protivnik napotezu))))) ;;izmedju stampaj i while petlje umetni setq trenutno stanje table 
    (progn (format t "~%Na potezu je ~a, unesite potez: ~%" napotezu )
     (let*((staro (read))
           (novo (read))
           (novatabla (promenitablu (cadr staro) (karakterubroj (car staro)) (cadr novo) (karakterubroj (car novo)) tabla napotezu)))
       (if (promenastanja staro novo tabla napotezu) (progn (format t "~%Tabla nakon poteza izgleda: ") (stampaj novatabla m) (whilepetlja novatabla (protivnik napotezu))) 
         (progn (format t "~%Nemoguc potez!~%") (whilepetlja tabla napotezu))))))))

;; OVAJ PISE HEURISTIKU, AKO NECES UZMI OVAJ GORE!
       

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;                                                          ;;;;;;;;;;;
;;;;;;;;;;;                     PHASE 2                              ;;;;;;;;;;;
;;;;;;;;;;;                                                          ;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;pomocna za proveru krajnjeg stanja
;heuristika za krajnje stanje treba da vrati +/- 1000

(defun pobednik (tabla napotezu)
  (cond ((null tabla) '())
        ((and (equal napotezu 'o) (or (member 'o (car tabla)) (not (nemafigura 'x tabla)))) t)
        ((and (equal napotezu 'x) (or (member 'x (car (last tabla))) (not (nemafigura 'o tabla)))) t)
        (t '())))



;; pomocna da proveri potez za kompa

(defun mogucastanjakomp (tabla napotezu x y)
  (cond	((ciljnaprovera tabla) '());;;PROVERITI DA LI OVDE CILJNAPROVERA ILI NIL, ranije bilo pobednik
        ((equal x m) '())
        ((equal (nth y (nth x tabla)) napotezu) (append (mogucipotezikomp tabla napotezu x y '0) (if (equal y (1- n)) (mogucastanjakomp tabla napotezu (1+ x) 0) (mogucastanjakomp tabla napotezu x (1+ y)))))
        (t (if (equal y (1- n)) (mogucastanjakomp tabla napotezu (1+ x) 0) (mogucastanjakomp tabla napotezu x (1+ y))))))
			


(defun mogucipotezikomp (tabla napotezu x y brojac)
  (if (equal napotezu 'x)
      (cond ((zerop brojac) (if (promenastanjakomp x y (1+ x) (1- y) tabla napotezu) (append (list (promenitablu x y (1+ x) (1- y) tabla napotezu)) (mogucipotezikomp tabla napotezu x y (1+ brojac))) (mogucipotezikomp tabla napotezu x y (1+ brojac))))
            ((equal brojac '1) (if (promenastanjakomp x y (1+ x) y tabla napotezu) (append (list (promenitablu x y (1+ x) y tabla napotezu)) (mogucipotezikomp tabla napotezu x y (1+ brojac))) (mogucipotezikomp tabla napotezu x y (1+ brojac))))
            ((equal brojac '2) (if (promenastanjakomp x y (1+ x) (1+ y) tabla napotezu) (list (promenitablu x y (1+ x) (1+ y) tabla napotezu))))
            (t '()))
    (cond ((zerop brojac) (if (promenastanjakomp x y (1- x) (1- y) tabla napotezu) (append (list (promenitablu x y (1- x) (1- y) tabla napotezu)) (mogucipotezikomp tabla napotezu x y (1+ brojac))) (mogucipotezikomp tabla napotezu x y (1+ brojac))))
          ((equal brojac '1) (if (promenastanjakomp x y (1- x) y tabla napotezu) (append (list (promenitablu x y (1- x) y tabla napotezu)) (mogucipotezikomp tabla napotezu x y (1+ brojac))) (mogucipotezikomp tabla napotezu x y (1+ brojac))))
          ((equal brojac '2) (if (promenastanjakomp x y (1- x) (1+ y) tabla napotezu) (list (promenitablu x y (1- x) (1+ y) tabla napotezu))))
          (t '()))))

(defun promenastanjakomp (x1 y1 x2 y2 tabla Trenutnoigra)
  (cond ((or (> y1 (1- n)) (< y1 '0)) '())
        ((or (> y2 (1- n)) (< y2 '0)) '())
        ((or (> x1 (1- m)) (< x1 '0)) '())
        ((or (> x2 (1- m)) (< x2 '0)) '())
        (t (cond ((and (equal y1 y2) (equal (nth y2 (nth x2 tabla)) '-)) t)
                 ((and (not (equal y1 y2)) (not (equal Trenutnoigra (nth y2 (nth x2 tabla))))) t)
				 (t '())))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;                                                          ;;;;;;;;;;;
;;;;;;;;;;;                     PHASE 3                              ;;;;;;;;;;;
;;;;;;;;;;;                                                          ;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;                                                          ;;;;;;;;;;;
;;;;;;;;;;;          Min-Max sa Alfa-Beta odsecanjem                 ;;;;;;;;;;;
;;;;;;;;;;;                                                          ;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; pomocna, menja not...
(defun protivnik (napotezu)
  (if (equal napotezu 'x) 'o 'x))

(defun max-value (novastanja dubina alfa beta napotezu najstanje)
  (cond ((null novastanja) (list najstanje alfa))
        (t (let* ((min-stanje (minimax-alfabeta (car novastanja) (1- dubina) alfa beta (protivnik napotezu)))
                  (novoalfa (apply 'max (list alfa (cadr min-stanje))))
                  (novonajstanje (if (> novoalfa alfa)  (car novastanja) najstanje)))
             (if (< novoalfa beta) (max-value (cdr novastanja) dubina novoalfa beta napotezu novonajstanje) (list novonajstanje novoalfa)))))) ;;; ako neki maksimum prebaci betu, njega ce beta u gornjem niovu svakako da izbaci
                                                                                                                     ;;; samim  tim dalja izracunavanja trenutnog maksimuma nisu bitna


(defun min-value (novastanja dubina alfa beta napotezu najstanje)
  (cond ((null novastanja) (list najstanje beta))
        (t (let* ((max-stanje (minimax-alfabeta (car novastanja) (1- dubina) alfa beta (protivnik napotezu)))
                  (novobeta (apply 'min (list beta (cadr max-stanje))))
                  (novonajstanje (if (< novobeta beta) (car novastanja) najstanje)))
             (if (> novobeta alfa) (min-value (cdr novastanja) dubina alfa novobeta napotezu novonajstanje) (list novonajstanje novobeta)))))) 


(defun minimax-alfabeta (stanje dubina alfa beta napotezu)
  (cond ((zerop dubina)  (list stanje (heuristika stanje napotezu)));;kad napravimo heuristiku promeni ovo
        (t (let ((novastanja (mogucastanjakomp stanje napotezu '0 '0)))
             (cond ((null novastanja) (list stanje (heuristika stanje napotezu)))
                   (t (if (equal napotezu 'x) (max-value novastanja dubina alfa beta napotezu '()) (min-value novastanja dubina alfa beta napotezu '()))))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;                                                          ;;;;;;;;;;;
;;;;;;;;;;;                     PHASE 4                              ;;;;;;;;;;;
;;;;;;;;;;;                                                          ;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;(defun proceni-stanje (stanje)
;;  (random 1000))

(defun heuristika (stanje napotezu);;od (0,0) krece
  (cond ((pobednik stanje 'x) '1000);;obrnuto ako je na potezu x u ciljnom stanju, pobedio je y  //pozvati pobednik za kompa
        ((pobednik stanje 'o) '-1000)
        (t (progn
             (defparameter *T1-FACTS* (pripremicinjenice stanje napotezu 0 0))
             (prepare-knowledge *T1-RULES* *T1-FACTS* 10)
             (if (equal napotezu 'x) 
                 (+ (maximzax stanje napotezu '0 '0) (* 4 (razlikafigura stanje napotezu))                     (* 0.3 (count-results '(Xvoz))) (if (> (count-results '(Siguricax)) 0) 900 0)
                    (if (and (> (count-results '(Xjeskorodopobede)) 0) (< (count-results '(Xnaputuugrozava)) 2)) 900 0))               
               (+ (maxim stanje napotezu '0) (* 4 (- 0 (razlikafigura stanje napotezu))) (- 0 (* 0.2 (count-results '(Osebrani))))
                  (- 0 (* 0.3 (count-results '(Ovoz)))) (if (> (count-results '(Siguricao)) 0) -900 0)
                  (if (and (> (count-results '(Ojeskorodopobede)) 0) (< (count-results '(Onaputuugrozava)) 2)) -900 0)))))))
            

(defun maxim (stanje napotezu brojac)
  (cond ((null stanje) brojac)
        ((member napotezu (car stanje)) (if (equal napotezu 'x) (- m brojac) (- brojac m)))
        (t (maxim (cdr stanje) napotezu (1+ brojac)))))

(defun maximzax (stanje napotezu brojac brojacvrste)
  (cond ((null stanje) brojac)
        ((member napotezu (car stanje)) (maximzax (cdr stanje) napotezu brojacvrste (1+ brojacvrste)))
        (t (maximzax (cdr stanje) napotezu brojac (1+ brojacvrste)))))

                
(defun razlikafigura (stanje napotezu)
  (- (- (brojfigura stanje napotezu) (brojfigura trenutnostanje napotezu)) (- (brojfigura stanje (protivnik napotezu)) (brojfigura trenutnostanje (protivnik napotezu)))))

;;(defun proveriugrozenost (stanje napotezu x y)
;; (cond ((equal x m) 0)
;;        ((and (equal (nth y (nth x stanje)) 'x) (or (if (> y 0) (equal (nth (1- y) (nth (1+ x) stanje)) (protivnik 'x)) NIL) (if (< y 7) (equal (nth (1+ y) (nth (1+ x) stanje)) (protivnik 'x)) NIL))) 
;;         (if (equal napotezu komp) '7 '-7))  
;;         ((and (equal (nth y (nth x stanje)) 'o) (or (if (> y 0) (equal (nth (1- y) (nth (1- x) stanje)) (protivnik 'o)) NIL) (if (< y 7) (equal (nth (1+ y) (nth (1- x) stanje)) (protivnik 'o)) NIL)))
;;         (if (equal napotezu komp) '-7 '7))
;;        (t (proveriugrozenost stanje napotezu (if (equal (1+ y) n) (1+ x) x) (if (equal (1+ y) n) '0 (1+ y))))))





(defun brojfigura (stanje znak)
  (cond ((null stanje) '0)
        ((list (car stanje)) (+ (brojfiguravrste (car stanje) znak) (brojfigura (cdr stanje) znak)))
        (t (brojfigura stanje znak))))

(defun brojfiguravrste (vrsta znak)
  (cond ((null vrsta) '0)
        ((equal (car vrsta) znak) (1+ (brojfiguravrste (cdr vrsta) znak)))
        (t (brojfiguravrste (cdr vrsta) znak))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;                                                                                                ;;;;;
;;;;;                                       INFERENCE ENGINE                                         ;;;;;
;;;;;                                                                                                ;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defparameter *T1-RULES* '(
                           (if (and (Pozicija x ?x1 ?y1) (Pozicija o ?x2 ?y2) (!dole ?x1 ?x2) (!levodesno ?y1 ?y2)) then (Ugrozavanje))
                           (if (and (Pozicija x ?x1 ?y1) (Pozicija x ?x2 ?y2) (!levodesno ?y1 ?y2) (!dole ?x1 ?x2)) then (Xsebrani))
                           (if (and (Pozicija o ?x1 ?y1) (Pozicija o ?x2 ?y2) (!levodesno ?y1 ?y2) (!gore ?x1 ?x2)) then (Osebrani))
                           (if (and (Pozicija o 1 ?y1) (Pozicija o 2 ?y2) (!levodesno ?y1 ?y2) then (Ojeskorodopobede))) ;; ako nas neko brani na korak do pobede a pritom nas napadaju manje od 2
                           (if (and (Pozicija o 1 ?y1) (Pozicija x 0 ?y2) (!levodesno ?y1 ?y2) then (Onaputuugrozava))) ;; mi sigurno pobedjujemo samo treba da se dobra razmena odradi...
                           (if (and (Pozicija x 6 ?y1) (Pozicija x 5 ?y2) (!levodesno ?y1 ?y2) then (Xjeskorodopobede)))
                           (if (and (Pozicija x 6 ?y1) (Pozicija 0 7 ?y2) (!levodesno ?y1 ?y2) then (Xnaputuugrozava))) 
                           (if (and (Pozicija x ?x1 ?y1) (Pozicija x ?x2 ?y2?) (!eq ?y1 ?y2) (!dole ?x1 ?x2)) then (Xvoz))
                           (if (and (Pozicija o ?x1 ?y1) (Pozicija o ?x2 ?y2?) (!eq ?y1 ?y2) (!gore ?x1 ?x2)) then (Ovoz))
                           ;;ne zanima nas gde se oni ugrozavaju, samo je bitno da postoji ako postoji pri nasem potezu onda extra, ako je pri njegovom onda je lose
                           (if (and (Ugrozavanje) (Napotezu x)) then (Xugrozava)) 
                           (if (and (Ugrozavanje) (Napotezu o)) then (Ougrozava))
                           (if (and (Pozicija x ?x1 ?y1) (!siguricazax ?x1 ?y1 0 0)) then (Siguricax)) ;;ako nema nikog u trouglu ispred sebe sigurno pobedjuje
                           (if (and (Pozicija o ?x1 ?y1) (!siguricazao ?x1 ?y1 0 0)) then (Siguricao))))

(defun pripremicinjenice (stanje napotezu x y)
  (cond	((equal x m) (cons (list 'Napotezu napotezu) '()))
        ((equal (nth y (nth x stanje)) 'x) (cons (list 'Pozicija 'x x y) (pripremicinjenice stanje napotezu (if (equal (1+ y) n) (1+ x) x) (if (equal (1+ y) n) 0 (1+ y)))))
        ((equal (nth y (nth x stanje)) 'o) (cons (list 'Pozicija 'o x y) (pripremicinjenice stanje napotezu (if (equal (1+ y) n) (1+ x) x) (if (equal (1+ y) n) 0 (1+ y)))))
        (t (pripremicinjenice stanje napotezu (if (equal (1+ y) n) (1+ x) x) (if (equal (1+ y) n) 0 (1+ y))))))

   
                   







;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; POMOCNE FUNKCIJE
                               
(defun !eq (a b)
  (equal a b))
;;ne zanimaju nas granicni slucajevi jel samo trazimo da li nesto postoji da bi znali da li smo ugrozeni
(defun !dole (a b) ;;uporedjujemo xeve ovim kada igra x
  (equal (1+ a) b))
(defun !gore (a b) ;;uporedjujemo yone ovim kada igra o
  (equal (1- a) b))
(defun !levodesno (a b) ;;kada bilo ko igra...
  (or (equal (1- a) b) (equal (1+ a) b)))

;;(defun !minus (a b)
;;  (- a b))

;;(defun !stevinoperator (x1 y1 x2 y2)
;;  (and (< (- y1 (- x2 x1)) y2) (< y2 (+ y1 (- x2 x1)))))



(defun !siguricazax (x1 y1 x2 y2)
  (cond ((equal x2 m) t)
        ((and (< (- y1 (- x2 x1)) y2) (< y2 (+ y1 (- x2 x1))) (clanp (list 'Pozicija 'o x2 y2) *t1-facts*)) NIL)
        (t (!siguricazax x1 y1 (if (equal (1+ y2) n) (1+ x2) x2) (if (equal (1+ y2) n) 0 (1+ y2))))))
(defun !siguricazao (x1 y1 x2 y2)
  (cond ((equal x2 m) t)
        ((and (< (- y1 (- x1 x2)) y2) (< y2 (+ y1 (- x1 x2))) (clanp (list 'Pozicija 'x x2 y2) *t1-facts*)) NIL)
        (t (!siguricazao x1 y1 (if (equal (1+ y2) n) (1+ x2) x2) (if (equal (1+ y2) n) 0 (1+ y2))))))

(defun clanp (cvor cvorovi)
  (cond ((null cvorovi) '())
        ((equal cvor (car cvorovi)) t)
        (t (clanp cvor (cdr cvorovi)))))

;; provera da li je parametar s izvorna promenljiva (simbol koji pocinje sa ?)
(defun true-var? (s) 
  (if (symbolp s)
      (equal #\? (char (symbol-name s) 0))
    nil))

;; provera da li je parametar s promenljiva (simbol koji pocinje sa ? ili %)
(defun var? (s) 
  (if (symbolp s)
      (let ((c (char (symbol-name s) 0)))
        (or (equal c #\?) (equal c #\%)))
    nil))

;; provera da li je parametar s funkcija (simbol koji pocinje sa =)
(defun func? (s) 
  (if (symbolp s)
      (equal #\= (char (symbol-name s) 0))
    nil))

;; provera da li je parametar s predefinisani predikat (simbol koji pocinje sa !)
(defun predefined-predicate? (s)
  (if (symbolp s)
      (equal #\! (char (symbol-name s) 0))
    nil))

;; provera da li je parametar s konstanta (ako nije promenljiva ili funkcija onda je konstanta)
(defun const? (s)
  (not (or (var? s) (func? s))))

;; rekurzivna provera da li je parametar f funkcija od parametra x
(defun func-of (f x)
  (cond
   ((null f) ; kraj rekurzije
    t)
   ((atom f)
    (equal f x))
   (t
    (or (func-of (car f) x) (func-of (cdr f) x)))))

;; provera da li funkcija f ima promenljivih
(defun has-var (f)
  (cond
   ((null f) 
    nil)
   ((atom f)
    (var? f))
   (t
    (or (has-var (car f)) (has-var (cdr f))))))

;; funkcija koja vraca konsekvencu pravila
(defun rule-consequence (r)
  (car (last r)))

;; funkcija koja vraca premisu pravila
(defun rule-premises (r)
  (let ((p (cadr r)))
    (if (and (listp p) (equal (car p) 'and))
        (cdr p)
      (list p))))
      
;; funkcija koja vrsi prebacivanje upita u interni format (izbacuje 'and)
(defun format-query (q)
  (if (and (listp q) (equal (car q) 'and))
      (cdr q)
    (list q)))
    
;; izracunavanje istinitosne vrednosti predefinisanog predikata
(defun evaluate-predicate (p ls)
  (if (has-var p) nil  ; ako poseduje slobodne promenljive vraca nil (nije validna situacija)
    (if (eval p) 
        (list ls) ; ako predikat vazi vraca ulaznu listu smena
      nil))) ; u suprotnom vraca nil

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; INTERFEJSNE FUNKCIJE I GLOBALNE PROMENLJIVE

(defparameter *FACTS* nil)
(defparameter *RULES* nil)
(defparameter *MAXDEPTH* 10)

;; priprema *FACTS*, *RULES* i *MAXDEPTH*
(defun prepare-knowledge (lr lf maxdepth)
  (setq *FACTS* lf *RULES* (fix-rules lr) *MAXDEPTH* maxdepth))

;; vraca broj rezulata izvodjenja
(defun count-results (q)
  (length (infer- (format-query q) '(nil) 0)))

;; vraca listu lista smena
(defun infer (q)
  (filter-results (infer- (format-query q) '(nil) 0)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FUNKCIJE KOJE VRSE DODELU NOVIH JEDINSTVENIH PROMENLJIVIH PRAVILIMA

(defun fix-rules (lr)
  (if (null lr) nil
    (cons (fix-rule (car lr)) (fix-rules (cdr lr)))))

(defun fix-rule (r)
  (let ((ls (make-rule-ls r nil)))
    (apply-ls r ls)))

(defun make-rule-ls (r ls)
  (cond
   ((null r)
    ls)
   ((var? r)
    (let ((a (assoc r ls)))
      (if (null a)
          (cons (list r (gensym "%")) ls)
        ls)))
   ((atom r)
    ls)   
   (t
    (make-rule-ls (cdr r) 
                  (make-rule-ls (car r) ls)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FUNKCIJE KOJE VRSE PRIPREMU REZULTATA (IZBACUJU SMENE KOJE SE ODNOSE NA INTERNE PROMENLJIVE)

(defun filter-results (lls)
  (if (null lls) nil
    (cons (filter-result (car lls)) (filter-results (cdr lls)))))

(defun filter-result (ls)
  (if (null ls) nil
    (if (true-var? (caar ls))
        (cons (car ls) (filter-result (cdr ls)))
      (filter-result (cdr ls)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FUNKCIJE KOJE SE KORISTE U IZVODJENJU

;; glavna funkcija za izvodjenje, vraca listu lista smena
;; lq - predikati upita
;; lls - lista listi smena (inicijalno lista koja sadrzi nil)
;; depth - tekuca dubina (inicijalno 0)
(defun infer- (lq lls depth)
  (if (null lq) lls
    (let ((lls-n (infer-q (car lq) lls depth)))
      (if (null lls-n) nil
        (infer- (cdr lq) lls-n depth)))))

;; izvodjenje za jedan predikat iz upita, vraca listu lista smena
(defun infer-q (q lls depth)
  (if (null lls) nil
    (let ((lls-n (infer-q-ls q (car lls) depth)))
      (if (null lls-n)
          (infer-q q (cdr lls) depth)
        (append lls-n (infer-q q (cdr lls) depth))))))

;; izvodjenje za jedan predikat sa jednom listom smena, vraca listu lista smena
(defun infer-q-ls (q ls depth)
  (if (predefined-predicate? (car q))
      (evaluate-predicate (apply-ls q ls) ls)
    (if (< depth *MAXDEPTH*)
        (append (infer-q-ls-lf q *FACTS* ls) (infer-q-ls-lr q *RULES* ls depth))
      (infer-q-ls-lf q *FACTS* ls))))
      
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; izvodjenje nad bazom cinjenica lf, vraca listu lista smena
(defun infer-q-ls-lf (q lf ls)
  (if (null lf) nil
    (let ((ls-n (infer-q-ls-f q (car lf) ls)))
      (if (null ls-n)
          (infer-q-ls-lf q (cdr lf) ls)
        (if (null (car ls-n)) ls-n
          (append ls-n (infer-q-ls-lf q (cdr lf) ls)))))))

;; izvodjenje sa jednom cinjenicom, vraca listu sa listom smena
(defun infer-q-ls-f (q f ls)
  (if (= (length q) (length f)) ; provera na istu duzinu
      (infer-q-ls-f- q f ls)
    nil))

;; izvodjenje sa jednom cinjenicom, vraca listu sa listom smena
(defun infer-q-ls-f- (q f ls)
  (if (null q) (list ls)
    (let ((nq (apply-and-eval (car q) ls)) (nf (car f)))
      (if (var? nq) 
          (infer-q-ls-f- (cdr q) (cdr f) (append ls (list (list nq nf))))
        (if (equal nq nf) 
            (infer-q-ls-f- (cdr q) (cdr f) ls)
          nil)))))
          
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; izvodjenje nad bazom pravila, vraca listu lista smena
(defun infer-q-ls-lr (q lr ls depth)
  (if (null lr) nil
    (let ((ls-n (infer-q-ls-r q (car lr) ls depth)))
      (if (null ls-n)
          (infer-q-ls-lr q (cdr lr) ls depth)
        (if (null (car ls-n)) ls-n
          (append ls-n (infer-q-ls-lr q (cdr lr) ls depth)))))))

;; izvodjenje sa jednim pravilom, vraca listu sa listom smena
(defun infer-q-ls-r (q r ls depth)
  (let ((c (rule-consequence r)))
    (if (= (length q) (length c))
        (let ((lsc (unify q c nil ls)))
          (if (null lsc) nil
            (infer- (apply-ls (rule-premises r) (car lsc)) (cdr lsc) (1+ depth))))
      nil)))

;; unifikacija predikata upita q i konsekvence pravila c primenom liste smena ls, vraca listu smena
(defun unify (q c uls ls)
  (if (or (null q) (null c))
      (if (and (null q) (null c)) (list uls ls) nil)
    (let ((eq (car q)) (ec (car c)))
      (cond
       ((equal eq ec)
        (unify (cdr q) (cdr c) uls ls))
       ((var? eq)
        (cond
         ((var? ec)
          (let ((a (assoc ec uls)))
            (cond
             ((null a)              
              (unify (cdr q) (cdr c) (cons (list ec eq) uls) ls))
             ((equal (cadr a) eq)
              (unify (cdr q) (cdr c) uls ls))
             (t
              nil))))
         ((func? ec)
          nil)
         (t ;; const
          (let ((a (assoc eq ls)))
            (cond
             ((null a)
              (unify (cdr q) (cdr c) uls (cons (list eq ec) ls)))
             ((equal (cadr a) ec)
              (unify (cdr q) (cdr c) uls ls))
             (t 
              nil))))))
       ((func? eq)
        (cond
         ((var? ec)
          (if (func-of eq ec) nil
            (let ((a (assoc ec uls)))
              (cond
               ((null a)              
                (unify (cdr q) (cdr c) (cons (list ec eq) uls) ls))
               ((equal (cadr a) eq)
                (unify (cdr q) (cdr c) uls ls))
               (t
                nil)))))
         ((func? ec)
          nil)
         (t ;; const
          (let ((f (apply-ls eq ls)))
            (if (has-var f) nil
              (if (equal (eval f) ec)
                  (unify (cdr q) (cdr c) uls ls)
                nil))))))
       (t ;; const
        (cond
         ((var? ec)
          (let ((a (assoc ec uls)))
            (cond
             ((null a)              
              (unify (cdr q) (cdr c) (cons (list ec eq) uls) ls))
             ((equal (cadr a) eq)
              (unify (cdr q) (cdr c) uls ls))
             (t
              nil))))
         (t ;; func or const
          nil)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PRIMENA LISTE SMENA I IZRACUNAVANJE IZRAZA

(defun apply-and-eval (x ls)
  (if (var? x)
      (apply-ls x ls)
    (if (and (listp x) (func? (car x)))
        (eval (apply-ls x ls)) 
      x)))

;; primena liste smena ls na izraz x
(defun apply-ls (x ls)
  (cond
   ((null x)
    x)
   ((var? x)
    (let ((ax (assoc x ls)))
      (if (null ax) x
        (cadr ax))))
   ((atom x)
    x)
   (t
    (cons (apply-ls (car x) ls) (apply-ls (cdr x) ls)))))

;;(setq matrica (konstruktor '8 '8 '8))

;;(setq m '8)

;;(setq n '8)

;;(car (minimax-alfabeta matrica '7 -1000 1000 'x))


